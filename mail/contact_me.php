<?php
// Check for empty fields

$spamfilter = $_POST['url'];
//there's a hidden field that will only be filled by bots. If it's not empty, it's a bot.
if ($spamfilter) {
  die(1);
}
$name = trim(filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
$email_address = trim($_POST['email']);
$phone = "no phone provided";
if (empty($_POST['phone'])) {
	$phone = "no phone provided";
}
else {
	$phone = trim(filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
}
$message = trim($_POST['message']);

if (filter_var($email_address, FILTER_VALIDATE_EMAIL)) {

	// Create the email and send the message
	$to = 'inquiry@safebreach.com';
	$email_subject = "Website Contact Form";
	$email_body = "A new message received from SafeBreach website contact form.\n\n"."Here are the details:\n\nName: $name\n\nEmail: $email_address\n\nPhone: $phone\n\nMessage:\n$message";
	$headers = "From: noreply@safebreach.com\n";
	$headers .= "Reply-To: $email_address";
	mail($to,$email_subject,$email_body,$headers);
	return true;
} else {
	return false;
}
?>
